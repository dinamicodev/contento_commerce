core = 7.x
api = 2

; COMMERCE:

projects[commerce][version] = 1.9
projects[commerce][subdir] = "contrib"
projects[commerce][patch][] = "http://drupal.org/files/issues/issue-2229901.patch"

projects[commerce_add_to_cart_confirmation][subdir] = "contrib"
projects[commerce_add_to_cart_confirmation][version] = 1.0-rc2

projects[commerce_addressbook][version] = 2.0-rc7
projects[commerce_addressbook][subdir] = "contrib"

projects[commerce_backoffice][subdir] = "contrib"
projects[commerce_backoffice][version] = 1.4

projects[commerce_autosku][version] = 1.x-dev
projects[commerce_autosku][subdir] = "contrib"

projects[commerce_checkout_progress][version] = 1.3
projects[commerce_checkout_progress][subdir] = "contrib"

projects[commerce_discount][version] = 1.x-dev
projects[commerce_discount][subdir] = "contrib"

projects[commerce_extra_price_formatters][version] = 1.x-dev
projects[commerce_extra_price_formatters][subdir] = "contrib"

projects[commerce_fancy_attributes][version] = 1.0
projects[commerce_fancy_attributes][subdir] = "contrib"

projects[commerce_features][version] = 1.0
projects[commerce_features][subdir] = "contrib"
projects[commerce_features][patch][] = "http://drupal.org/files/1402762_export_flat_rate_commerce_features-6.patch"

projects[commerce_feeds][version] = 1.3
projects[commerce_feeds][subdir] = "contrib"

projects[commerce_flat_rate][version] = 1.0-beta2
projects[commerce_flat_rate][subdir] = "contrib"

projects[commerce_message][subdir] = "contrib"
projects[commerce_message][version] = 1.0-rc1
projects[commerce_message][patch][] = "http://drupal.org/files/issues/commerce_message-fix-multilingual-message-types-2033815-3.patch"

projects[commerce_migrate][version] = 1.1
projects[commerce_migrate][subdir] = "contrib"

projects[commerce_physical][version] = 1.x-dev
projects[commerce_physical][subdir] = "contrib"

projects[commerce_search_api][subdir] = "contrib"
projects[commerce_search_api][version] = 1.3

projects[commerce_shipping][version] = 2.1
projects[commerce_shipping][subdir] = "contrib"

projects[currency_commerce][type] = "module"
projects[currency_commerce][version] = "1.1"
projects[currency_commerce][subdir] = "contrib"


; FEATURES:

projects[contento_commerce_features][type] = "module"
projects[contento_commerce_features][download][type] = "get"
projects[contento_commerce_features][download][url] = "https://bitbucket.org/dinamico/contento_commerce_features/get/master.zip"
